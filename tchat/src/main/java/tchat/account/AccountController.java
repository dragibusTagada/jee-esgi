package tchat.account;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import tchat.message.Message;
import tchat.message.MessageController;
import tchat.message.MessageUser;
import tchat.message.TokenGenerator;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

/**
 * Gère tous les chemins utilisant AccountRepository ou InMemoryUserDetailsManager
 */
@Controller
public class AccountController extends WebMvcConfigurerAdapter
{
    @Autowired
    AccountRepository accountRepository;

    /* Permet de stocker les comptes pour la sécurité de l'application */
    private final InMemoryUserDetailsManager inMemoryUserDetailsManager;

    @Autowired
    public AccountController(InMemoryUserDetailsManager inMemoryUserDetailsManager)
    {
        this.inMemoryUserDetailsManager = inMemoryUserDetailsManager;
    }

    public AccountController()
    {
        inMemoryUserDetailsManager = null;
    }

    /* Sert pour la récupération des messages du tchat sur l'affichage REST. */
    @Autowired
    MessageController messageController;

    /*______________________________________________________________________________________________________________________________________________________*/


    /**************** Ajout User ****************/
    /**----------------------------------**/
    /**----------------------------------**/

    /* Ajoute un Utilisateur en prenant en paramètre un Account */
    @RequestMapping(value = "add", method = RequestMethod.POST)
    public String add(@Valid @ModelAttribute Account account, BindingResult bindingResult)
    {
        if(!(bindingResult.hasErrors()))    //Si il n'y a pas d'erreurs (défini par les règles liés à Account).
        {
            if(!inMemoryUserDetailsManager.userExists(account.getUsername()))   //Si l'user n'existe pas déjà.
            {
                accountRepository.save(account);    //Ajoute un account en memoire dans JPA.
                inMemoryUserDetailsManager.createUser(new User(account.getUsername(), account.getPassword(), new ArrayList<GrantedAuthority>())); //Ajoute un utilisateur en mémoire dans le service "securing a web application" de spring.io
            }
            else //Sinon l'username existe déjà.
            {
                bindingResult.rejectValue("username", "error.account", "Login already exists.");    // On ajoute une erreur à username.
            }
        }
        return "login";
    }

    /**----------------------------------**/
    /**----------------------------------**/
    /**----------------------------------**/

    /*______________________________________________________________________________________________________________________________________________________*/

    /**************** Login ****************/
    /**----------------------------------**/
    /**----------------------------------**/

    /* Affichage des formulaires de connexion et inscription */
    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String LoginForm(Model model)
    {
        model.addAttribute("account", new Account()); // Permet d'instancier un Account vide pour le formulaire d'inscription
        return "login";
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST) // Permet de se connecter, en utilisant "securing a web application" de spring.io.
    public String LoginSubmit(@Valid @ModelAttribute Account account, BindingResult bindingResult)
    {
        return "login";
    }

    /**----------------------------------**/
    /**----------------------------------**/
    /**----------------------------------**/

    /*______________________________________________________________________________________________________________________________________________________*/

    /**************** Tchat ****************/
    /**----------------------------------**/
    /**----------------------------------**/

    /* Tchat */
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String GreetingForm(Model model, HttpServletRequest request)
    {
        List<Account> account = accountRepository.findByUsername(request.getRemoteUser()); /*Récupère l'account grâce au nom de l'utilisateur récupéré via HttpServletRequest. */
        account.get(0).setToken(TokenGenerator.generateToken());    /* Mets un Jeton dans le compte afin de pouvoir vérifier s'il n'y a pas d'usurpation à l'envoi des messages par la suite. */
        accountRepository.save(account.get(0));    /* Sauvegarde l'account avec le nouveau jeton. */
        model.addAttribute("account", account.get(0));  /* Met l'account dans la variable account. */
        model.addAttribute("messageRepo", messageController.messageRepository.findAll());
        return "index";
    }

    /* Permet de récupérer les messages envoyés du côté client depuis le côté client. */
    @MessageMapping("/tchat")
    @SendTo("/topic/greetings")
    public MessageUser messageUser(Message message) throws Exception
    {
        if(accountRepository.findByUsername(message.getUser()).get(0).getToken().equals(message.getToken())) /* Vérification du token utilisateur avec le jeton stocker dans account afin de vérifier qu'il n'y a pas d'usurpation. */
        {
            MessageUser messageUser = new MessageUser("<img src='" + message.getUser() + "' width='25px' height='25px'/> " + message.getUser() + " : " + message.getMessage()); /* Créatien du message au format d'affichage voulu. */
            messageController.messageRepository.save(messageUser);    /* Sauvegarde le message pour pouvoir le récupérer avec REST. */
            return messageUser; /* Retourne le message. */
        }
        else /* Si le jeton utilisateur ne correspond pas à celui dans account on ne fait rien */
        {
            return null;
        }
    }

    /**----------------------------------**/
    /**----------------------------------**/
    /**----------------------------------**/

    /*______________________________________________________________________________________________________________________________________________________*/
}
