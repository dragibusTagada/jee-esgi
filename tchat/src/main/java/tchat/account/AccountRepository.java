package tchat.account;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import java.util.List;

/**
 * Stockage des Account
 */
@EnableJpaRepositories
public interface AccountRepository extends JpaRepository<Account, Long>
{
    List<Account> findByUsername(String username);
}
