package tchat.account;

import lombok.*;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Classe qui permet de créer un Account contenant :
 *  - Un id auto généré
 *  - Un username de taille mini 3 et maxi 10
 *  - Un password de taille mini 3
 *  - Un token afin de vérifier l'identité de la personne lors de l'émission des messages du tchat
 */
@Entity
@Getter
@Setter
@NoArgsConstructor
public class Account
{
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private long id;

    @NotNull
    @Size(min=3, max=10)
    private String username;

    @NotNull
    @Size(min=3, max=10)
    private String password;

    private String token;

    public Account(String username, String password, String token) {
        this.username = username;
        this.password = password;
        this.token = token;
    }

    public Account(String username, String password) {
        this.username = username;
        this.password = password;

    }
}
