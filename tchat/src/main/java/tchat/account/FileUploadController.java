package tchat.account;

import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import tchat.TchatApplication;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;

/**
 * Classe qui gère l'upload de fichier (uniquement png et jpg)
 * A COMMENTER !!!!!!!!!!!!!!!
 */
@Controller
public class FileUploadController
{
    @RequestMapping(method = RequestMethod.POST, value = "/upload")
    public String handleFileUpload(@RequestParam("name") String name, @RequestParam("file")MultipartFile file, RedirectAttributes redirectAttributes)
    {
        if (name.contains("/")) {
            redirectAttributes.addFlashAttribute("message", "Folder separators not allowed");
            return "redirect:";
        }

        if (name.contains("/")) {
            redirectAttributes.addFlashAttribute("message", "Relative pathnames not allowed");
            return "redirect:";
        }

        if (!file.getContentType().equalsIgnoreCase("image/jpeg") && !file.getContentType().equalsIgnoreCase("image/png")) {
            redirectAttributes.addFlashAttribute("message", "Files must be png or jpg");
            return "redirect:";
        }

        if(!file.isEmpty())
        {
            try
            {
                BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(new File(TchatApplication.ROOT + "/" + name)));
                FileCopyUtils.copy(file.getInputStream(),stream);
                stream.close();
                redirectAttributes.addFlashAttribute("message", "You successfully uploaded " + name + "!");
            }
            catch (Exception e)
            {
                redirectAttributes.addFlashAttribute("message", "You failed to upload " + name + " => " + e.getMessage());
            }
        }
        else {
            redirectAttributes.addFlashAttribute("message",
                    "You failed to upload " + name + " because the file was empty");
        }

        return "redirect:";
    }
}
