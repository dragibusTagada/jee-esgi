package tchat.message;

/**
 * Génération aléatoire de Jetons/Tokens
 */
import java.security.SecureRandom;
import java.math.BigInteger;

public final class TokenGenerator {
    private static SecureRandom random = new SecureRandom();

    public static String generateToken() {
        return new BigInteger(130, random).toString(32);
    }
}
