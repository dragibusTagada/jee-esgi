package tchat.message;

import lombok.Getter;

/**
 * Classe qui permet de créer des Message
 */
@Getter
public class Message
{
    private String message;
    private String user;
    private String token;
}
