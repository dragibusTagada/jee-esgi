package tchat.message;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Classe qui permet de récupérer le tchat en Json en Rest via l'url /messages
 */
@RestController
public class MessageController {

    @Autowired
    @Setter
    @Getter
    public MessageRepository messageRepository;

    @RequestMapping("/messages")
    public List<MessageUser> messages()
    {
        return messageRepository.findAll();
    }
}
