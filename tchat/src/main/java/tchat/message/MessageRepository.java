package tchat.message;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Stockage des MessageUser
 */
public interface MessageRepository extends JpaRepository<MessageUser, Long>
{
}
