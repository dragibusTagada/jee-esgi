package tchat.message;

import lombok.Getter;
import lombok.NoArgsConstructor;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Classe qui permet de créer un MessageUser, contient :
 * - Un id auto généré
 * - Un String
 */
@Entity
@NoArgsConstructor
@Getter
public class MessageUser
{
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private long id;

    private String content;

    public MessageUser(String content) {
        this.content = content;
    }

}
