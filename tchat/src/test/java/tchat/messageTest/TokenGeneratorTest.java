package tchat.messageTest;

import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static tchat.message.TokenGenerator.generateToken;

/**
 * Created by MonCompte on 17/04/2016.
 */
public class TokenGeneratorTest
{
    @Test
    public void should_create_a_token_of_26_chars(){
        String token = generateToken();
        assertThat(token.length(), is(26));
    }
}
