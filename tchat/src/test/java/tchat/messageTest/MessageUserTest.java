package tchat.messageTest;

import org.junit.Test;
import tchat.message.MessageUser;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

/**
 * Created by MonCompte on 17/04/2016.
 */
public class MessageUserTest
{
    @Test
    public void should_create_messageUser_with_id(){
        MessageUser messageUser = new MessageUser("test");

        assertThat(messageUser.getId(), is(0L));
    }

    @Test
    public void should_create_messageUser_with_content(){
        MessageUser messageUser = new MessageUser("test");

        assertThat(messageUser.getContent(), is("test"));
    }

    @Test
    public void should_create_messageUser_with_an_empty_content(){
        MessageUser messageUser = new MessageUser("");

        assertThat(messageUser.getContent(), is(""));
    }
}
