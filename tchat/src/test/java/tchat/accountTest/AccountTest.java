package tchat.accountTest;

import org.junit.Test;
import tchat.account.Account;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class AccountTest {

    @Test
    public void should_create_account_with_username(){
        Account account = new Account("test", "password");

        assertThat(account.getUsername(), is("test"));
    }

    @Test
    public void should_create_account_with_password(){
        Account account = new Account("test", "password");

        assertThat(account.getUsername(), is("password"));
    }



    @Test
    public void add_account(){
        String name = "azerty";
        String pwd = "mdp";
        Account account = new Account(name, pwd);
        account.setUsername("drake");

        assertThat(account.getUsername(), is("drake"));
        assertThat(account.getPassword(), is("mdp"));
        //verify(accountController).add(account, bindingResult);
    }


}
